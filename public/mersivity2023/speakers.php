<h1><tt><strong>IEEE WaterHCI / Mersivity 2023 Symposium, 2023 December 14th, MC102, University of Toronto</strong></tt></h1>

<ul>
<li>
Host:	Steve Mann, MIT '97, P. Eng. Ontario, widely regarded as the father of wearable computing, inventor of HDR, co-founder of InteraXon, makers of the Muse brain-sensing headband.<br/>
<img width=160 src=http://wearcam.org/headshot.jpg>
<li>
Host:	Yu Yuan, President of the IEEE Standards Association.</br>
<img width=160 src=YuYuan_headshot_1516460924878.jpeg>
<li>
	Photographer: Reid Godshaw, American actor, filmmaker, Grammy Awards<br>
<img width=160 src=ReidGodshaw_wwrqyqe4wnvseqwr.jpg>

<li>
Avi Bar-Zeev has been at the forefront of Spatial Computing for over 30 years,
	launching Disney's groundbreaking Aladdin VR ride in the early 90s, crafting Second Life's 3D worlds
	and co-founding Keyhole (Google Earth) mirror-earth 3D browser (around 2001).
	He co-invented the Microsoft Hololens AR headset in 2010, helped define Amazon Echo Frames in 2015,
	and led the Experience Prototyping team for Apple's Vision Pro from 2016 to 2019.
Most recently, he is a founder and President of The XR Guild, a new non-profit membership organization that seeks to support and educate professionals in XR on ethics and most positive outcomes for humanity.
<img width=160 src=Avi_Bar-Zeev_headshot.png>

</ul>
Summer Mersivity Symposium speakers (Tue. Aug. 15, 10am, Michael Hough Beach, Ontario Place, West Island):
<ul>
<li>Mike Schreiner, Party leader of the Green Party of Ontario.<br/>
<img width=160 src=Mike_Schreiner-2021_Wikimedia_cropped.jpg>(Wikipedia)
<li> Charles Rishor, Yachting Director, Boulevard Yacht Club.<br/>
<img width=160 src=CharlesRishor1628361163626.jpeg>
<li>Niv Froehlich, Director of Paddle Canada Stand Up Paddleboard &amp; Kayak Programs.  "Creating a Paddle-Friendly City"<br/>
<img width=160 src=Niv-Froehlich-Bio-Photo.jpg>
<li>David Shellnutt, "The Biking Lawyer".<br/>
<img width=160 src=DavidShellnutt_bio_picture_CyhFRz15_400x400.jpg>
</ul>

<hr>
2022 speakers:
<br/>
<img width=720 src=IEEE_Mersivity_Davos.png>

