function toggleDropdown() {
    var menu = document.getElementById("menuItems");
    if (menu.style.display === "none") {
        menu.style.display = "block";
    } else {
        menu.style.display = "none";
    }
}
